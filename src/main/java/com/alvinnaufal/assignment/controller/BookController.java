package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.dto.book.BookCustomSelectDto;
import com.alvinnaufal.assignment.dto.book.BookMappedDto;
import com.alvinnaufal.assignment.model.Book;
import com.alvinnaufal.assignment.projection.book.BookInfo;
import com.alvinnaufal.assignment.service.BookService;
import com.alvinnaufal.assignment.util.dto.Response;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/book")
public class BookController {
    @Autowired
    private BookService bookService;

    @PostMapping
    public ResponseEntity<ResponseDto<BookMappedDto>> save(@RequestBody Book book) {
        ResponseDto<BookMappedDto> response =
                Response.responseDtoSuccess(bookService.save(book));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Invalid input")
    })
    @GetMapping
    public ResponseEntity<ResponseDto<List<BookMappedDto>>> findAll() {
        ResponseDto<List<BookMappedDto>> response =
                Response.responseDtoSuccess(bookService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/custom")
    public ResponseEntity<ResponseDto<List<BookCustomSelectDto>>> findAllCustomSelect() {
        ResponseDto<List<BookCustomSelectDto>> response =
                Response.responseDtoSuccess(bookService.findAllCustomSelect());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/count-author")
    public ResponseEntity<ResponseDto<List<BookInfo>>> countAuthor() {
        ResponseDto<List<BookInfo>> response =
                Response.responseDtoSuccess(bookService.countAuthor());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Optional<BookMappedDto>>> delete(
            @PathVariable Long id) {
        Optional<BookMappedDto> data = bookService.softDelete(id);
        ResponseDto<Optional<BookMappedDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
