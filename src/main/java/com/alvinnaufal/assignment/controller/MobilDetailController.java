package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.dto.mobilDetail.MobilDetailDto;
import com.alvinnaufal.assignment.service.MobilDetailService;
import com.alvinnaufal.assignment.util.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/mobil/detail")
public class MobilDetailController {
    @Autowired
    private MobilDetailService mobilDetailService;

    @GetMapping
    public ResponseEntity<ResponseDto<Iterable<MobilDetailDto>>> findAll() {
        ResponseDto<Iterable<MobilDetailDto>> response =
                Response.responseDtoSuccess(mobilDetailService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<Optional<IDto>>> findOne(@PathVariable(name = "id") Long mobilDetailID) {
        Optional<IDto> data = mobilDetailService.findOne(mobilDetailID);
        ResponseDto<Optional<IDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ResponseDto<Optional<IDto>>> update(
            @RequestBody MobilDetailDto mobilDetailDto) {
        Optional<IDto> data = mobilDetailService.update(mobilDetailDto);
        ResponseDto<Optional<IDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
