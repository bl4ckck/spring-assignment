package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.mobil.MobilDto;
import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.model.Mobil;
import com.alvinnaufal.assignment.service.MobilService;
import com.alvinnaufal.assignment.util.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/mobil")
public class MobilController {
    @Autowired
    private MobilService mobilService;

    @PostMapping
    public ResponseEntity<ResponseDto<MobilDto>> save(@Valid @RequestBody Mobil mobil) {
        ResponseDto<MobilDto> response =
                Response.responseDtoSuccess(mobilService.save(mobil));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<ResponseDto
            <Iterable<MobilDto>>> findAll(@RequestParam Map<String,String> params) {
        ResponseDto<Iterable<MobilDto>> response =
                Response.responseDtoSuccess(mobilService.findAll(params));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<Optional<MobilDto>>> findOne(
            @PathVariable(name = "id") Long mobilID) {
        Optional<MobilDto> data = mobilService.findOne(mobilID);
        ResponseDto<Optional<MobilDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public Mobil update(@RequestBody Mobil mobil, @PathVariable Long id) {
        return mobilService.update(mobil, id);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Optional<MobilDto>>> delete(
            @PathVariable Long id) {
        Optional<MobilDto> data = mobilService.softDelete(id);
        ResponseDto<Optional<MobilDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
