package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.model.Role;
import com.alvinnaufal.assignment.service.RoleService;
import com.alvinnaufal.assignment.util.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @GetMapping
    public ResponseEntity<ResponseDto<List<Role>>> findAllRole() {
        ResponseDto<List<Role>> response =
                Response.responseDtoSuccess(roleService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResponseDto<Role>> save(@RequestBody Role role) {
        ResponseDto<Role> response =
                Response.responseDtoSuccess(roleService.save(role));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
