package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.dto.chapter.ChapterMappedDto;
import com.alvinnaufal.assignment.service.ChapterService;
import com.alvinnaufal.assignment.util.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/chapter")
public class ChapterController {
    @Autowired
    private ChapterService chapterService;

    @GetMapping
    public ResponseEntity<ResponseDto<Iterable<ChapterMappedDto>>> findAll() {
        ResponseDto<Iterable<ChapterMappedDto>> response =
                Response.responseDtoSuccess(chapterService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/name")
    public ResponseEntity<ResponseDto<Iterable<ChapterMappedDto>>> findByName(@RequestParam String name) {
        ResponseDto<Iterable<ChapterMappedDto>> response =
                Response.responseDtoSuccess(chapterService.findByName(name));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Optional<ChapterMappedDto>>> delete(
            @PathVariable Long id) {
        Optional<ChapterMappedDto> data = chapterService.softDelete(id);
        ResponseDto<Optional<ChapterMappedDto>> response;
        if (data.isPresent()) response = Response.responseDtoSuccess(data);
        else response = Response.responseDtoFailed(data);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
