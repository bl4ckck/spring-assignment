package com.alvinnaufal.assignment.controller;

import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.model.User;
import com.alvinnaufal.assignment.service.UserService;
import com.alvinnaufal.assignment.util.dto.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<ResponseDto<List<User>>> findAllUser() {
        ResponseDto<List<User>> response =
                Response.responseDtoSuccess(userService.findAll());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/query")
    public ResponseEntity<ResponseDto<User>> findByUsername(@RequestParam String username) {
        ResponseDto<User> response =
                Response.responseDtoSuccess(userService.findByUsername(username));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ResponseDto<User>> saveUser(@RequestBody User user) {
        ResponseDto<User> response =
                Response.responseDtoSuccess(userService.save(user));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
