package com.alvinnaufal.assignment.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@Table(name = "mobil")
@NamedQueries({
        @NamedQuery(name = "Mobil.findByIsDeleted", query = "select m from Mobil m where m.isDeleted = :isDeleted"),
        @NamedQuery(name = "findByIsDeleted", query = "select m from Mobil m where m.isDeleted = :isDeleted")
})
public class Mobil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
    @JoinColumn(name = "mobil_detail_id", nullable = false)
    private MobilDetail mobilDetail;

    @NotNull
    @Column(name = "brand")
    private String brand;

    @NotNull
    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Mobil mobil = (Mobil) o;
        return id != null && Objects.equals(id, mobil.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}