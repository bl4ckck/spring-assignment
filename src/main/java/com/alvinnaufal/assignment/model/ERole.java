package com.alvinnaufal.assignment.model;

public enum ERole {
    ROLE_ADMIN,
    ROLE_CUSTOMER
}
