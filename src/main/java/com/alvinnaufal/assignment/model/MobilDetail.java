package com.alvinnaufal.assignment.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Setter
@Getter
@ToString
@Entity
@Table(name = "mobil_detail")
public class MobilDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne(mappedBy = "mobilDetail", orphanRemoval = true)
    private Mobil mobil;

    @NotNull
    private String color;

    @NotNull
    @Column(name = "`isNew`")
    private Boolean isNew;

    @NotNull
    private Integer year;

    @NotNull
    private Double price;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MobilDetail that = (MobilDetail) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}