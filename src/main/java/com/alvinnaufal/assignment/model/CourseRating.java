package com.alvinnaufal.assignment.model;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "course_rating")
public class CourseRating {
    @EmbeddedId
    private CourseRatingKey id;

    @ManyToOne
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    private Course course;

    private Integer rating;
}