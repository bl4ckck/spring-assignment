package com.alvinnaufal.assignment.config;

import com.alvinnaufal.assignment.controller.BookController;
import com.alvinnaufal.assignment.dto.ResponseDto;
import com.alvinnaufal.assignment.dto.book.BookMappedDto;
import io.swagger.annotations.ApiResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

//@ComponentScan(basePackageClasses = {
//        BookController.class
//})
@EnableSwagger2
@EnableWebMvc
@Profile("dev")
@Configuration
public class SpringFoxConfig {
    @Bean
    public Docket bookApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
//                .paths(regex("/api/v1/book/*"))
                .build();
//                .forCodeGeneration(true);
    }
}
