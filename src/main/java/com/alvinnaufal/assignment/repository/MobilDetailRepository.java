package com.alvinnaufal.assignment.repository;

import com.alvinnaufal.assignment.model.MobilDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MobilDetailRepository extends JpaRepository<MobilDetail, Long> {
    Optional<MobilDetail> findByMobil_Id(Long id);
}