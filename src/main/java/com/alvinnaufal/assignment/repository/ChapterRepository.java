package com.alvinnaufal.assignment.repository;

import com.alvinnaufal.assignment.model.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ChapterRepository extends JpaRepository<Chapter, Long> {
    List<Chapter> findByName(String name);
}