package com.alvinnaufal.assignment.repository;

import com.alvinnaufal.assignment.model.ERole;
import com.alvinnaufal.assignment.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}