package com.alvinnaufal.assignment.repository;

import com.alvinnaufal.assignment.dto.book.BookCustomSelectDto;
import com.alvinnaufal.assignment.model.Book;
import com.alvinnaufal.assignment.projection.book.BookInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

//class columnBuilder {
//    public BookMappedDto.BookMappedDtoBuilder customSelect() {
//        BookMappedDto.BookMappedDtoBuilder builder = BookMappedDto
//                .builder()
//                .id()
//                .author()
//                .title()
//                .build();
//    }
//}

public interface BookRepository extends JpaRepository<Book, Long> {
//    @Query(value = "select b.id, b.author as author, " +
//            "b.title as title, b.chapters as chapter " +
//            "from Book b " +
//            "inner join fetch Chapter c on b.id = c.book.id", nativeQuery = false)
//    List<BookCustomSelectProjection> findAllCustomSelect();
    @Query(value = "SELECT " +
            "new com.alvinnaufal.assignment.dto.book.BookCustomSelectDto (b.id, b.author," +
            " b.title, c.name, c.content) " +
            "FROM Book b " +
            "LEFT JOIN Chapter c on b.id = c.book.id")
    List<BookCustomSelectDto> findAllCustomSelect();

    @Query(value = "SELECT b.author AS author, " +
            "count(b.id) AS total " +
            "FROM Book b GROUP BY b.author")
    List<BookInfo> countTotalBookByAuthor();
}