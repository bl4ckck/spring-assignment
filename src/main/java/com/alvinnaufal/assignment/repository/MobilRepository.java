package com.alvinnaufal.assignment.repository;

import com.alvinnaufal.assignment.model.Mobil;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MobilRepository extends JpaRepository<Mobil, Long> {
    List<Mobil> findByIsDeleted(Boolean isDeleted);
}