package com.alvinnaufal.assignment.service;

import com.alvinnaufal.assignment.dto.book.BookCustomSelectDto;
import com.alvinnaufal.assignment.dto.book.BookMappedDto;
import com.alvinnaufal.assignment.mapper.book.BookMappedMapper;
import com.alvinnaufal.assignment.model.Book;
import com.alvinnaufal.assignment.model.Chapter;
import com.alvinnaufal.assignment.projection.book.BookInfo;
import com.alvinnaufal.assignment.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookMappedMapper mapper;

    public BookMappedDto save(Book book) {
        List<Chapter> newChapter = book.getChapters().stream()
                .peek(val -> val.setBook(book))
                .collect(Collectors.toList());
        book.setChapters(newChapter);
        Book create = bookRepository.save(book);
        return mapper.toDto(create);
    }

    public List<BookMappedDto> findAll() {
        List<Book> bookList = bookRepository.findAll();
        return mapper.toList(bookList);
    }

    public List<BookCustomSelectDto> findAllCustomSelect() {
        return bookRepository.findAllCustomSelect();
    }

    public List<BookInfo> countAuthor() {
        return bookRepository.countTotalBookByAuthor();
    }

    public Optional<BookMappedDto> softDelete(Long id) {
        Optional<BookMappedDto> result = Optional.empty();
        Optional<Book> data = bookRepository
                .findById(id)
                .map(item -> {
                    item.setIsDeleted(Boolean.TRUE);
                    return bookRepository.save(item);
                });
        if (data.isPresent()) {
            result = Optional.of(mapper.toDto(data.get()));
        }
        return result;
    }
}
