package com.alvinnaufal.assignment.service;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.mobil.MobilDto;
import com.alvinnaufal.assignment.dto.mobilDetail.MobilDetailDto;
import com.alvinnaufal.assignment.mapper.mobilDetail.MobilDetailMapper;
import com.alvinnaufal.assignment.model.MobilDetail;
import com.alvinnaufal.assignment.repository.MobilDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MobilDetailService {
    @Autowired
    private MobilDetailRepository mobilDetailRepository;

    @Autowired
    private MobilDetailMapper mapper;


    public Iterable<MobilDetailDto> findAll() {
        List<MobilDetail> mobilDetailList = mobilDetailRepository.findAll();
        return mapper.toList(mobilDetailList);
    }

    public Optional<IDto> findOne(Long id) {
        Optional<MobilDetail> mobilDetail = mobilDetailRepository.findById(id);
        IDto mobilDto = new MobilDto();
        if (mobilDetail.isPresent()) mobilDto =
                mapper.toDto(mobilDetail.get());
        return Optional.of(mobilDto);
    }

    public Optional<IDto> update(MobilDetailDto mobilDetailDto) {
        Long mobilID = mobilDetailDto.getMobilId();
        Optional<IDto> result = Optional.empty();
        Optional<MobilDetail> data = mobilDetailRepository
                .findByMobil_Id(mobilID)
                .map(item -> {
                    item.setColor(mobilDetailDto.getColor());
                    item.setYear(mobilDetailDto.getYear());
                    item.setIsNew(mobilDetailDto.getIsNew());
                    item.setPrice(mobilDetailDto.getPrice());
                    return mobilDetailRepository.save(item);
                });
        if (data.isPresent()) {
            result = Optional.of(mapper.toDto(data.get()));
        }
        return result;
    }
}
