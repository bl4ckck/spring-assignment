package com.alvinnaufal.assignment.service;

import com.alvinnaufal.assignment.dto.chapter.ChapterMappedDto;
import com.alvinnaufal.assignment.mapper.chapter.ChapterMappedMapper;
import com.alvinnaufal.assignment.model.Chapter;
import com.alvinnaufal.assignment.repository.ChapterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChapterService {
    @Autowired
    private ChapterRepository chapterRepository;

    @Autowired
    private ChapterMappedMapper mapper;

    public List<ChapterMappedDto> findAll() {
        List<Chapter> chapterList = chapterRepository.findAll();
        return mapper.toList(chapterList);
    }

    public List<ChapterMappedDto> findByName(String name) {
        List<Chapter> chapterList = chapterRepository.findByName(name);
        return mapper.toList(chapterList);
    }

    public Optional<ChapterMappedDto> softDelete(Long id) {
        Optional<ChapterMappedDto> result = Optional.empty();
        Optional<Chapter> data = chapterRepository
                .findById(id)
                .map(item -> {
                    item.setIsDeleted(Boolean.TRUE);
                    return chapterRepository.save(item);
                });
        if (data.isPresent()) {
            result = Optional.of(mapper.toDto(data.get()));
        }
        return result;
    }
}
