package com.alvinnaufal.assignment.service;

import com.alvinnaufal.assignment.dto.mobil.MobilDto;
import com.alvinnaufal.assignment.mapper.mobil.MobilMapper;
import com.alvinnaufal.assignment.model.Mobil;
import com.alvinnaufal.assignment.repository.MobilRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class MobilService {
    @Autowired
    private MobilRepository mobilRepository;

    @Autowired
    private MobilMapper mapper;

    public MobilDto save(Mobil mobil) {
        mobil.getMobilDetail().setMobil(mobil);
        Mobil create = mobilRepository.save(mobil);
        return mapper.toDto(create);
    }

    public Iterable<MobilDto> findAll(Map<String, String> params) {
        List<Mobil> mobilList = mobilRepository.findAll();
        if (params.get("isDeleted") != null) {
            return this.findAllIsDelete(
                    Boolean.valueOf(params.get("isDeleted"))
            );
        }
        return mapper.toList(mobilList);
    }

    public Iterable<MobilDto> findAllIsDelete(Boolean isDeleted) {
        List<Mobil> mobilList = mobilRepository.findByIsDeleted(isDeleted);
        return mapper.toList(mobilList);
    }

    public Optional<MobilDto> findOne(Long id) {
        Optional<Mobil> mobil = mobilRepository.findById(id);
        MobilDto mobilDto = new MobilDto();
        if (mobil.isPresent())
            mobilDto = mapper.toDto(mobil.get());
        return Optional.of(mobilDto);
    }

    public Mobil update(Mobil mobil, Long id) {
        return null;
    }

    public void delete(Long id) {
        mobilRepository.deleteById(id);
    }

    public Optional<MobilDto> softDelete(Long id) {
        Optional<MobilDto> result = Optional.empty();
        Optional<Mobil> data = mobilRepository
                .findById(id)
                .map(item -> {
                    item.setIsDeleted(Boolean.TRUE);
                    return mobilRepository.save(item);
                });
        if (data.isPresent()) {
            result = Optional.of(mapper.toDto(data.get()));
        }
        return result;
    }
}
