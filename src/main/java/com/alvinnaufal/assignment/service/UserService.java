package com.alvinnaufal.assignment.service;

import com.alvinnaufal.assignment.model.Role;
import com.alvinnaufal.assignment.model.User;
import com.alvinnaufal.assignment.repository.RoleRepository;
import com.alvinnaufal.assignment.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Transactional
    public User save(User user) {
        Set<Role> roles = user.getRoles().stream().peek(val -> {
            Optional<Role> role = roleRepository
                    .findByName(val.getName());
            role.ifPresent(value -> val.setId(value.getId()));
        }).collect(Collectors.toSet());
        user.setRoles(roles);
        return userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByUsername(String username) {
        Optional<User> data = userRepository.findByUsername(username);
        return data.orElse(null);
    }
}
