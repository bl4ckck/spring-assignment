package com.alvinnaufal.assignment.mapper.book;

import com.alvinnaufal.assignment.dto.book.BookMappedDto;
import com.alvinnaufal.assignment.mapper.chapter.ChapterMapper;
import com.alvinnaufal.assignment.model.Book;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { ChapterMapper.class })
public interface BookMappedMapper {
    BookMappedMapper INSTANCE = Mappers.getMapper( BookMappedMapper.class );

    @Mapping(source = "chapters", target = "chapter")
    BookMappedDto toDto(Book book);

    List<BookMappedDto> toList(List<Book> book);
}
