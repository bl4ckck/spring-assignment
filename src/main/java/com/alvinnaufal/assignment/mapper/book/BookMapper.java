package com.alvinnaufal.assignment.mapper.book;

import com.alvinnaufal.assignment.dto.book.BookDto;
import com.alvinnaufal.assignment.dto.book.BookMappedDto;
import com.alvinnaufal.assignment.mapper.chapter.ChapterMapper;
import com.alvinnaufal.assignment.model.Book;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {
    BookMapper INSTANCE = Mappers.getMapper( BookMapper.class );

    BookDto toDto(Book book);
}
