package com.alvinnaufal.assignment.mapper.mobil;

import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import com.alvinnaufal.assignment.dto.mobil.MobilDto;
import com.alvinnaufal.assignment.mapper.mobilDetail.MobilDetailMapper;
import com.alvinnaufal.assignment.model.Chapter;
import com.alvinnaufal.assignment.model.Mobil;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { MobilDetailMapper.class })
public interface MobilMapper {
    MobilMapper INSTANCE = Mappers.getMapper( MobilMapper.class );

    MobilDto toDto(Mobil mobil);

    List<MobilDto> toList(List<Mobil> mobil);
}
