package com.alvinnaufal.assignment.mapper.mobilDetail;

import com.alvinnaufal.assignment.dto.mobil.MobilDto;
import com.alvinnaufal.assignment.dto.mobilDetail.MobilDetailDto;
import com.alvinnaufal.assignment.model.Mobil;
import com.alvinnaufal.assignment.model.MobilDetail;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MobilDetailMapper {
    MobilDetailMapper INSTANCE = Mappers.getMapper( MobilDetailMapper.class );

    @Mapping(source = "mobil.id", target = "mobilId")
    MobilDetailDto toDto(MobilDetail mobilDetail);

    List<MobilDetailDto> toList(List<MobilDetail> mobilDetails);
}
