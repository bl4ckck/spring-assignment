package com.alvinnaufal.assignment.mapper.chapter;

import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import com.alvinnaufal.assignment.dto.chapter.ChapterMappedDto;
import com.alvinnaufal.assignment.mapper.book.BookMapper;
import com.alvinnaufal.assignment.model.Chapter;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { BookMapper.class })
public interface ChapterMappedMapper {
    ChapterMappedMapper INSTANCE = Mappers.getMapper( ChapterMappedMapper.class );

    ChapterMappedDto toDto(Chapter chapter);

    List<ChapterMappedDto> toList(List<Chapter> chapter);
}
