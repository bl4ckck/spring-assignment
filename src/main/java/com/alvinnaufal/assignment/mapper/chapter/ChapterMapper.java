package com.alvinnaufal.assignment.mapper.chapter;

import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import com.alvinnaufal.assignment.model.Chapter;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ChapterMapper {
    ChapterMapper INSTANCE = Mappers.getMapper( ChapterMapper.class );

    @Mapping(source = "book.id", target = "bookId")
    ChapterDto toDto(Chapter chapter);
}
