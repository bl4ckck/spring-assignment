package com.alvinnaufal.assignment.dto.mobilDetail;

import com.alvinnaufal.assignment.dto.IDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class MobilDetailDto implements Serializable, IDto {
    private Long id;
    private Long mobilId;
    private String color;
    private Boolean isNew;
    private Integer year;
    private Double price;
}
