package com.alvinnaufal.assignment.dto.mobil;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.mobilDetail.MobilDetailDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class MobilDto implements Serializable, IDto {
    private Long id;
    private String brand;
    private Boolean isDeleted;
    private MobilDetailDto mobilDetail;
}
