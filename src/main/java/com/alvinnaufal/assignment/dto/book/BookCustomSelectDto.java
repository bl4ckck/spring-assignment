package com.alvinnaufal.assignment.dto.book;

import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import com.alvinnaufal.assignment.model.Book;
import com.alvinnaufal.assignment.model.Chapter;
import com.alvinnaufal.assignment.projection.book.BookCustomSelectProjection;
import lombok.ToString;
import lombok.Value;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Value
class ChapterCustom {
    String name;
    String content;
}

@Value
public class BookCustomSelectDto {
    Long id;
    String author;
    String title;
    ChapterCustom chapter;

    public BookCustomSelectDto(Long id, String author, String title,
                               String chapterName, String chapterContent) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.chapter = new ChapterCustom(
                chapterName,
                chapterContent);
    }
}
