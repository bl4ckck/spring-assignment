package com.alvinnaufal.assignment.dto.book;

import com.alvinnaufal.assignment.dto.IDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@NoArgsConstructor
@Data
public class BookSaveDto implements Serializable, IDto {
    private Long id;
    private String title;
    private String author;
    private Timestamp year;
    private Boolean isDeleted;
}
