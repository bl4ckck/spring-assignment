package com.alvinnaufal.assignment.dto.book;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@NoArgsConstructor
@Data
public class BookDto implements Serializable, IDto {
    private Long id;
    private String title;
    private String author;
    private Timestamp year;
    private Boolean isDeleted;
}
