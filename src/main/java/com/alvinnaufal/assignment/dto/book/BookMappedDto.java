package com.alvinnaufal.assignment.dto.book;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.chapter.ChapterDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BookMappedDto implements Serializable, IDto {
    private Long id;
    private List<ChapterDto> chapter;
    private String title;
    private String author;
    private Timestamp year;
    private Boolean isDeleted;
}
