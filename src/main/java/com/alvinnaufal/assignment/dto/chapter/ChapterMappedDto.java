package com.alvinnaufal.assignment.dto.chapter;

import com.alvinnaufal.assignment.dto.IDto;
import com.alvinnaufal.assignment.dto.book.BookDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class ChapterMappedDto implements Serializable, IDto {
    private Long id;
    private BookDto book;
    private String name;
    private String content;
    private Boolean isDeleted;
}
