package com.alvinnaufal.assignment.dto.chapter;

import com.alvinnaufal.assignment.dto.IDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class ChapterDto implements Serializable, IDto {
    private Long id;
    private Long bookId;
    private String name;
    private String content;
    private Boolean isDeleted;
}
