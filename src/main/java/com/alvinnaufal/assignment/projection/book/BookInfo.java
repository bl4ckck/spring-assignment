package com.alvinnaufal.assignment.projection.book;

public interface BookInfo {
    String getAuthor();
    Long getTotal();
}
