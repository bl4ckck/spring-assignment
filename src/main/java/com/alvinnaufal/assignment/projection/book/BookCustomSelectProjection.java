package com.alvinnaufal.assignment.projection.book;

import java.util.List;

public interface BookCustomSelectProjection {
    Long getId();
    String getAuthor();
    String getTitle();
    List<Chapter> getChapter();

    interface Chapter {
//        Long getId();
//        Boolean getIsDeleted();
        String getName();
        String getContent();
    }
}
