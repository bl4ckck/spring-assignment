package com.alvinnaufal.assignment.util.dto;

import com.alvinnaufal.assignment.dto.ResponseDto;
import org.springframework.http.HttpStatus;

public class Response {
    public static <T> ResponseDto<T> responseDtoSuccess(T data) {
        return new ResponseDto<T>(
                true,
                HttpStatus.OK.toString(),
                "Data Found",
                data
        );
    }

    public static <T> ResponseDto<T> responseDtoFailed(T data) {
        return new ResponseDto<T>(
                false,
                HttpStatus.OK.toString(),
                "Data Not Found",
                data
        );
    }
}